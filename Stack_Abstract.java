import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;


public class Stack_Abstract<E> extends AbstractCollection <E> {
	
	private int size = 0;
	private Object [] myObs = null;
	
	//Iterator
	private class myIterator<E> implements Iterator<E>{

		private int pos = 0;		
		myIterator(){}
		
		public boolean hasNext() {
			return (pos < size);
		}

		@SuppressWarnings("unchecked")
		public E next() 
		{
			if (pos < size)
				return (E) myObs[pos++];
			else
				throw new NullPointerException();
		}

		public void remove() {
			throw new UnsupportedOperationException("NO");
		}	
	}

	//Stack methods
	Stack_Abstract()
	{
	  myObs = new Object[size];
	}
	
	public boolean push(E elt)
	{
		if (!contains(elt))
		{
			size++;
			Object [] tObs = new Object[size];
			
			tObs[size-1] = elt;
			for (int i = 0; i < size - 1; i++)
				tObs[i] = myObs[i];
			
			myObs = tObs;
			return true;
		}
			
		return false;
	}
	
	public Object pop()
	{
		if (size > 0)
		{
			Object Elt = peek();
			size--;
			Object [] tObs = new Object [size];
			
			for (int i = 0; i < size; i++)
				tObs[i] = myObs[i];
			
			myObs = tObs;
				
			return Elt;	
		}
		else
			throw new NullPointerException("Stack size is 0.");
	}

	public Object peek()
	{
		return myObs[size-1];
	}
	
	public int search(Object arg0)
	{
		for (int i = 0; i < size; i++)
			if (arg0.equals(myObs[i]))
				return i;
		
		return -1;
	}
	
	//Collections methods
	public boolean add(E arg0) {
		return push(arg0);
	}
	
	public Iterator<E> iterator() {
		return (new myIterator<E>());
	}

	public int size() {
		return size;
	}

	//Overwrite methods; bind to normal behavior
	public boolean remove(Object arg0) {	
		 throw new UnsupportedOperationException("Does not support removal of objects in arbitraty manner.");
	}

	public boolean removeAll(Collection<?> arg0) {
		throw new UnsupportedOperationException("Does not support removal of objects in arbitraty manner.");
	}

	public boolean retainAll(Collection<?> arg0) {
		throw new UnsupportedOperationException("NO");
	}
}
