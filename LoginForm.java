import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

class LoginForm extends JFrame{
    
	private JTextField tf;
	private JPasswordField pf;
	private JButton submitBut, clearBut, exitBut;
	private JLabel  nameLabel, passwordLabel;
    
    LoginForm() {
	
    super("Andrew's Login Page");
	Container cp = getContentPane();
	cp.setLayout(new FlowLayout(FlowLayout.LEFT));

	//Labels
	nameLabel = new JLabel("login           ");
	cp.add(nameLabel);

	tf = new JTextField(10);
	cp.add(tf);
	
	passwordLabel = new JLabel("password");
	cp.add(passwordLabel);
	
	pf = new JPasswordField(10);
	cp.add(pf);

	//Buttons
	submitBut = new JButton("Submit");
	submitBut.addActionListener(new ActionListener() {
		
		public void actionPerformed(ActionEvent e) 
		{
			  String login = tf.getText();
			  char[] password = pf.getPassword();
			  String passwordAsString = new String(password);
		}
	});
	cp.add(submitBut);
	
	clearBut = new JButton("clear");
	clearBut.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) 
		{
			tf.setText(null);
			pf.setText(null);
		    return;
		}
	});
	cp.add(clearBut);

	exitBut = new JButton("exit");
	exitBut.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) 
		{
			System.exit(1);
		}
	});
	cp.add(exitBut);

	setSize(200,150);
	setVisible( true);
	
    }
    
    //Run the program
    public static void main(String[] args){
    	new LoginForm();
    }

}