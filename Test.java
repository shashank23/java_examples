import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Hashtable;
import java.util.LinkedList;

/*
 * Test multiple classes add and retrieval operations
 */

public class Test {
	
	public static void main(String [] args)
	{
		ArrayList <Integer>AList = new ArrayList <Integer>();
		LinkedList<Integer>LList = new LinkedList<Integer>();
		Hashtable<Integer, Integer> HTable = new Hashtable<Integer, Integer>();
		
		int Overall = 100000;
		Integer e = new Integer(10);
		int constPos = Overall/2;
		
		long time;
		int [] RandomList = new int[Overall];
	
		//Randomize a list for later use	
		for (int i = 0; i < Overall; i++)
			RandomList[i] = i;	
		Collections.shuffle(Arrays.asList(RandomList));
		
		/************* Insertions Operations *****************/
		System.out.println("Time to add " + Overall + " items.\n");
		
		//Populate the Hashtable with stuff
		time = System.nanoTime();
		for (int i = 0; i < Overall; i++)
			HTable.put(RandomList[i],i);
		time = (long) ((System.nanoTime() - time)/Math.pow(10,6));
		System.out.println("Time to populate Hashtable: " + time + " microseconds.\n");
		
		//ArrayList
		time = System.nanoTime();
		for (int i = 0; i < Overall; i++)
			AList.add(e);
		time = (long) ((System.nanoTime() - time)/Math.pow(10,6));
		System.out.println("ArrayList linear insertions, 0 inital capacity: " + time + " microseconds.");
		e = new Integer(10);
		
		AList = new ArrayList <Integer>(Overall);
		
		time = System.nanoTime();
		for (int i = 0; i < Overall; i++)
			AList.add(e);		
		time = (long) ((System.nanoTime() - time)/Math.pow(10,6));
		System.out.println("ArrayList linear insertions, " + Overall + " inital capacity: " + time + " microseconds.");
		e = new Integer(10);
		
		time = System.nanoTime();
		for (int i = 0; i < Overall; i++)
			AList.add(e, RandomList[i]);		
		time = (long) ((System.nanoTime() - time)/Math.pow(10,6));
		System.out.println("ArrayList random insertions, " + Overall + " inital capacity: " + time + " microseconds.");
		e = new Integer(10);
		
		System.out.println("");
		
		//LinkedList
		time = System.nanoTime();
		for (int i = 0; i < Overall; i++)
			LList.add(e);		
		time = (long) ((System.nanoTime() - time)/Math.pow(10,6));
		System.out.println("LinkedList linear insertions: " + time + " microseconds.");
		e = new Integer(10);
		
		time = System.nanoTime();
		for (int i = 0; i < Overall; i++)
			LList.add(e, RandomList[i]);
		time = (long) ((System.nanoTime() - time)/Math.pow(10,6));
		System.out.println("LinkedList random insertions: " + time + " microseconds.");
		e = new Integer(10);
		
		//Need to reshuffle the RandomList
		Collections.shuffle(Arrays.asList(RandomList));
		
		/************* Retrival Operations *****************/
		System.out.println("\nTime to retrieve items " + Overall + " times\n");
		
		//ArrayList
		time = System.nanoTime();
		for (int i = 0; i < Overall; i++)
			AList.get(constPos);
		time = (long) ((System.nanoTime() - time)/Math.pow(10,6));
		System.out.println("ArrayList constant position retrievals: " + time + " microseconds.");
		
		time = System.nanoTime();
		for (int i = 0; i < Overall; i++)
			AList.get(i);
		time = (long) ((System.nanoTime() - time)/Math.pow(10,6));
		System.out.println("ArrayList linear position retrievals: " + time + " microseconds.");
		
		time = System.nanoTime();
		for (int i = 0; i < Overall; i++)
			AList.get(RandomList[i]);
		time = (long) ((System.nanoTime() - time)/Math.pow(10,6));
		System.out.println("ArrayList random position retrievals: " + time + " microseconds.");
		
		System.out.println();
		
		//LinkedList
		time = System.nanoTime();
		for (int i = 0; i < Overall; i++)
			LList.get(constPos);
		time = (long) ((System.nanoTime() - time)/Math.pow(10,6));
		System.out.println("LinkedList constant position retrievals: " + time + " microseconds.");
		
		
		time = System.nanoTime();
		for (int i = 0; i < Overall; i++)
			LList.get(i);
		time = (long) ((System.nanoTime() - time)/Math.pow(10,6));
		System.out.println("LinkedList linear position retrievals: " + time + " microseconds.");
		
		
		time = System.nanoTime();
		for (int i = 0; i < Overall; i++)
			LList.get(RandomList[i]);
		time = (long) ((System.nanoTime() - time)/Math.pow(10,6));
		System.out.println("LinkedList random position retrievals: " + time + " microseconds.");
		
		System.out.println();
		
		//Hashtable
		time = System.nanoTime();
		for (int i = 0; i < Overall; i++)
			HTable.get(constPos);
		time = (long) ((System.nanoTime() - time)/Math.pow(10,6));
		System.out.println("Hashtable constant position retrievals: " + time + " microseconds.");

		time = System.nanoTime();
		for (int i = 0; i < Overall; i++)
			HTable.get(i);
		time = (long) ((System.nanoTime() - time)/Math.pow(10,6));
		System.out.println("Hashtable linear position retrievals: " + time + " microseconds.");
		
		time = System.nanoTime();
		for (int i = 0; i < Overall; i++)
			HTable.get(RandomList[i]);
		time = (long) ((System.nanoTime() - time)/Math.pow(10,6));
		System.out.println("Hashtable random position retrievals: " + time + " microseconds.");
		
	}
}
