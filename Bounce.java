import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Bounce extends JFrame{
	
	private Ball [] Balls;	
	private int HowManyBalls;
	
	private Color ballColor;
	private int diameter = 40;
	private int maxSpeed = 6;
	private int [][] pos;

	Random r = new Random();
	
	private Container contentPane = getContentPane();

	private JPanel window = new JPanel();
	private JPanel button = new JPanel();

	private Dimension d = new Dimension(1200,800);
	int height, width;
	
	private Thread T;
	
	private boolean keepGoing = true;
	private boolean alreadyStarted = false;
	
	private long timeDelay = 5;
	
	Bounce(int HowManyBalls)
	{
		this.HowManyBalls = HowManyBalls;
		Balls = new Ball[this.HowManyBalls];

		setSize(d);
		setTitle("Bounce");
		contentPane.add(window, "Center");
		
		width = d.width-10;
		height = d.height-70;
	
		Buttons();
	}
	
	private void Buttons()
	{
		JButton Start = new JButton("Start");
		Start.addActionListener( new ActionListener()
		{  
			public void actionPerformed(ActionEvent evt)
			{ 	
				if (!alreadyStarted)
				{
					//Initialize the Balls and Position Matrix
					pos = new int[width+1][height+1];
					for (int i = 0; i <= width; i++)
						for (int j = 0; j <= height; j++)
							pos[i][j] = -1;
					
					for (int i = 0; i < HowManyBalls; i++)
					{
						Balls[i] = new Ball(window, r.nextInt(diameter)+diameter);
						Balls[i].setColor(new Color(r.nextInt(255),r.nextInt(255),r.nextInt(255)));
						Balls[i].setID(i);
						
						Balls[i].dx((int)(Math.pow(-1,r.nextInt(1)))*(r.nextInt(maxSpeed-1)+1));
						Balls[i].dy((int)(Math.pow(-1,r.nextInt(1)))*(r.nextInt(maxSpeed-1)+1));
						
						Balls[i].move(r.nextInt(width-Balls[i].Diameter()), r.nextInt(height-Balls[i].Diameter()));
						
						setPosMatrix(Balls[i].X(), Balls[i].Y(), Balls[i].Diameter(), i);	
					}	
					
					T = new Thread(new Runnable(){
						public void run(){
							Move();
						}
					});
					T.start();
					
					alreadyStarted = true;
				}		
			}
		 });
		button.add(Start);
		
		
		final JButton Pause = new JButton("Pause");
		Pause.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				if (T.isAlive())
				{
					keepGoing = !keepGoing;
					
					if (!keepGoing)
						Pause.setText("UnPause");
					else
						Pause.setText("Pause");
				}
			}
		});
		button.add(Pause);
		
		JButton Stop = new JButton("Stop");
		Stop.addActionListener(new ActionListener()
		{  
			public void actionPerformed(ActionEvent evt)
			{ 
				System.exit(0);
			}
		 });
		button.add(Stop);
		
		contentPane.add(button, "South");
	}

	
	private void setPosMatrix(int X, int Y, int Diameter, int val)
	{
		for (int i = X; i <= X+Diameter; i++)
			for (int j = Y; j <= Y+Diameter; j++)
				pos[i][j] = val;
	}
	
	private void Move()
	{
		/*The point is that one ball moves at a time*/
		int X, Y, D;
		int dx, dy;
		int Xstep = 0, Ystep = 0;
		int k = 1, j = 1;
		
		//Move the Balls
		while (true){
			while(keepGoing){
				
				try {Thread.sleep(timeDelay);} 
				catch (InterruptedException e) {}
				
				Collections.shuffle(Arrays.asList(Balls));
				for (int i = 0; i < HowManyBalls; i++)
				{		
				    dx = Balls[i].dX();
					dy = Balls[i].dY();
					X = Balls[i].X();
					Y = Balls[i].Y();
					D = Balls[i].Diameter();
					Xstep = 0;	Ystep = 0;
					
					//Movement in the X direction
					if(Math.abs(dx) == dx)
					{
						for (k = 1; k <= dx; k++ )
							if (X+D+k <= width)
								for (j = 0; j <= D; j++)	
									if (pos[X+D+k][Y+j] == -1)
										Xstep = k;
									else
										{j = D+1; k = dx+1;}
					}
					else
					{
						for (k=1 ; k <= Math.abs(dx); k++)
							if (X-k >= 0)
								for (j = 0; j <= D; j++)	
									if (pos[X-k][Y+j] == -1)
										Xstep = -k;
									else
										{j=D+1; k = Math.abs(dx)+1;}
					}
					
					//Movement in the Y direction
					if(Math.abs(dy) == dy)
					{
						for (k = 1; k <= dy; k++ )
							if (Y+D+k <= height)
								for (j = 0; j <= D; j++)	
									if (pos[X+j][Y+D+k] == -1)
									Ystep = k;
								else
									{j = D+1; k = dy+1;}
					}
					else
					{
						for (k = 1; k <= Math.abs(dy); k++ )
							if (Y-k >= 0)
								for (j = 0; j <= D; j++)	
									if (pos[X+j][Y-k] == -1)
										Ystep = -k;	
									else
										{j = D+1; k = Math.abs(dy)+1;}
					}
		
					if (X+Xstep != X+dx)
						dx = -dx;
					if (Y+Ystep != Y+dy)
						dy = -dy;
					
					if (Y+Ystep != Y+dy || X+Xstep != X+dx) Balls[i].setColor(new Color(r.nextInt(255),r.nextInt(255),r.nextInt(255)));
					
					//Draws over the current position of the ball with a ball of the same size but the color of the background
					ballColor = Balls[i].myColor();
					
					Balls[i].setColor(window.getBackground());
					Balls[i].move(Balls[i].X(), Balls[i].Y());
					
					//Fill in the new positions in the matrix, and set the new dx and dy
					setPosMatrix(X, Y, D, -1);
					X += Xstep;
					Y += Ystep;
					setPosMatrix(X, Y, D, i+1);
					
					Balls[i].dy(dy);
					Balls[i].dx(dx);
				
					//Redraw the colored ball in the new position
					Balls[i].setColor(ballColor);
					Balls[i].move(X, Y);
					
					//Some fact about the ball
					window.getGraphics().drawString(String.valueOf(Balls[i].myID()), X+D/2-3, Y+D/2+3);
					
					window.getGraphics().dispose();		
				}	
			}
			System.out.println("Bye");
		}
	}
	//End of Move
}
	
