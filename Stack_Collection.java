import java.util.Collection;
import java.util.Iterator;


public class Stack_Collection<E> implements Collection<E>{
	
	private int size = 0;
	private Object [] myObs = null;
	
	//Iterator thing
	private class myIterator<E> implements Iterator<E>{

		private int pos = 0;		
		myIterator(){}
		
		public boolean hasNext() {
			return (pos < size);
		}

		@SuppressWarnings("unchecked")
		public E next() 
		{
			if (hasNext())
				return (E) myObs[pos++];
			else
				throw new NullPointerException();
		}

		public void remove() {
			throw new UnsupportedOperationException("NO");
		}	
	}
	
	//Stack methods
	Stack_Collection()
	{
	  myObs = new Object[size];
	}
	
	public boolean push(E elt)
	{
		if (!contains(elt))
		{
			size++;
			Object [] tObs = new Object[size];
			
			tObs[size-1] = elt;
			for (int i = 0; i < size - 1 ; i++)
				tObs[i] = myObs[i];
			
			myObs = tObs;
			return true;
		}
			
		return false;
	}
	
	public Object pop()
	{
		if (size > 0)
		{
			Object Elt = peek();
			size--;
			Object [] tObs = new Object [size];
			
			for (int i = 0; i < size; i++)
				tObs[i] = myObs[i];
			
			myObs = tObs;
			return Elt;	
		}
		else
			throw new NullPointerException("Stack size is 0.");
	}

	public Object peek()
	{
		return myObs[size-1];
	}
	
	public int search(Object arg0)
	{
		for (int i = 0; i < size; i++)
			if (arg0.equals(myObs[i]))
				return i;
		
		return -1;
	}
	
	//Collections methods
	public boolean add(E arg0) {
		return push(arg0);
	}
	
	public boolean addAll(Collection<? extends E> arg0) {
		int count = 0;
		while (arg0.iterator().hasNext())
			if (add(arg0.iterator().next()))
				count++;
		
		return (count > 0);
	}

	public void clear() {
		myObs = null;
	}

	public boolean contains(Object arg0) {
		return (search(arg0) != -1);
	}

	public boolean containsAll(Collection<?> arg0) {

		int count = 0;
		Iterator it = arg0.iterator();
		while (it.hasNext())
			if (contains(it.next()))
				count++;
		
		return (count == arg0.size());
	}

	public boolean isEmpty() {
		return (size < 1);
	}

	public Iterator<E> iterator() {
		return (new myIterator<E>()); 
	}

	public boolean remove(Object arg0) {
		 throw new UnsupportedOperationException("NO");
	}

	public boolean removeAll(Collection<?> arg0) {
		throw new UnsupportedOperationException("NO");
	}

	public boolean retainAll(Collection<?> arg0) {
		throw new UnsupportedOperationException("NO");
	}

	public int size() {
		return size;
	}

	public Object[] toArray() {
		
		return myObs;
	}

	@SuppressWarnings("unchecked")
	public <T> T[] toArray(T[] arg0) {
		
		if (size > 0)
		{	
			for (int i = 0; i < arg0.length; i++)
				if (i < size)
					arg0[i] = (T)myObs[i];
				else
					arg0[i] = null;
				
			return (T[])toArray();
		}		
		else
			throw new NullPointerException();
	}
}
