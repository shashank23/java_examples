
public class server{

}

/*

Andrew decided to add on this question to your homework 4 but as EXTRA CREDIT. 

7. You need to create an application that simulates (KEYWORD: simulates, which means we are not using RMI or Sockets here) client-server functionality in a multi-threaded environment. In your main class you will spawn one Server thread, then you will spawn n Client threads. The Server class should implement the Singleton design pattern (very simple, look it up). When a new Server thread is spawned it should wait until told otherwise. When the Server is notified, it will check if its message queue is not empty, then it will print the messages in the message queue to the screen, once it is done, if there are any Clients still signed in, then wait again. Once all clients sign off, then the server thread will end. When a Client thread is spawned, it will sign on to the Server, sends a random number of messages to the Server (less than 10), sleeping 50 milliseconds between every message send, then logs off the server.

Design:

Main:
- main method: spawns one Server thread and n Client threads

Server: (Singleton) 
- should have a global Client list and a global message list
- getInstance: returns Server, static
- loginClient: takes Client
- logoutClient: takes Client
- addMessage: take String message
- processMessage: prints the messages on the screen
- run: Server logic

Client: 
- run: Client logic


*/