import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class Ball {

	private int d = 10;
	private int x = 0, y = 0;
	private int dx = 2, dy = 2;

	private Color c = new Color(0, 0, 0);
	private Graphics Ball;

	private int id;
	private int CollisonCount = 0;

	Ball(JPanel Box, int r) {
		this.Ball = Box.getGraphics();
		this.d = r;
	}

	public void setColor(Color c) {
		this.c = c;
		Ball.setColor(this.c);
	}

	public void move(int x, int y) {
		this.x = x;
		this.y = y;

		Ball.fillOval(x, y, d, d);
	}

	public void setID(int i) {
		this.id = i;
	}

	public int myID() {
		return id;
	}

	public void CC(int C) {
		CollisonCount = C;
	}

	public void dx(int dx) {
		this.dx = dx;
	}

	public void dy(int dy) {
		this.dy = dy;
	}

	public Color myColor() {
		return c;
	}

	public int Diameter() {
		return d;
	}

	public int CC() {
		return CollisonCount;
	}

	public int dX() {
		return dx;
	}

	public int dY() {
		return dy;
	}

	public int X() {
		return x;
	}

	public int Y() {
		return y;
	}

}