import javax.swing.JFrame;


public class BounceMain {
	public static void main(String [] args)
	{
		int howManyBalls = 12;
		
		if (args.length == 1)
			howManyBalls = Integer.parseInt(args[0]);
		
		JFrame Frame = new Bounce(howManyBalls);
		Frame.setVisible(true);
		
	}
}
