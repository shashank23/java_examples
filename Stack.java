
public class Stack<E> {
	
	private int size = 0;
	private Object [] myObs = null;
	private Object [] tObs = null;

	public boolean push(E elt)
	{
		if (search(elt) == -1)
		{
			size++;
			tObs = new Object[size];
			
			tObs[size-1] = elt;
			for (int i = 0; i < size - 1 ; i++)
				tObs[i] = myObs[i];
			
			myObs = tObs;
			return true;
		}
			
		return false;
	}
	
	public Object pop()
	{
		if (size > 0)
		{
			Object Elt = peek();
			size--;
			Object [] tObs = new Object [size];
			
			for (int i = 0; i < size; i++)
				tObs[i] = myObs[i];
			
			myObs = tObs;
			
			return Elt;	
		}
		else
			throw new NullPointerException("Stack size is 0.");
	}

	public Object peek()
	{
		return myObs[size-1];
	}
	
	public int search(Object arg0)
	{
		for (int i = 0; i < size; i++)
			if (arg0.equals(myObs[i]))
				return i;
		
		return -1;
	}
	
	public int size()
	{
		return size;
	}
}
