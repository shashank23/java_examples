import java.util.Iterator;


public class StackMain {
	public static void main(String [] args)
	{
		Stack<Integer> Stack = new Stack<Integer>();
		Stack_Collection<Integer> StackCollection = new Stack_Collection<Integer>();
		Stack_Abstract<Integer> StackAbstract = new Stack_Abstract<Integer>();
		
		for (int i = 0 ; i < 7; i++)
		{
			Stack.push(i);
			StackCollection.push(i);
			StackAbstract.push(i);
		}
		
		System.out.println(Stack.pop());
		System.out.println(Stack.peek());
		System.out.println(StackCollection.search(3));
		
		System.out.println();
	
		Iterator<Integer> it = StackAbstract.iterator();
		while (it.hasNext())
			System.out.println(it.next());
	
	}
}
